#include <iostream>
#include <conio.h>
#include <string>
#include "Main.h"

using namespace std;

enum Rank {
	Two = 2,
	Three,
	Four, Five,
	Six, Seven,
	Eight, 
	Nine,
	Ten,
	Jack, 
	Queen, 
	King, 
	Ace
};

enum Suit {
	Spades = 1,
	Hearts,
	Clubs,
	Diamonds
};

enum Change {
	a = 1,
	b,
	c,
	d
};

struct Card
{
	Rank rank;
	Suit suit;
};

void PrintCard(Card card) {
	cout << "The ";
	
	switch(card.rank) {
		case Two: cout << "Two"; break;
		case Three: cout << "Three"; break;
		case Four: cout << "Four"; break;
		case Five: cout << "Five"; break;
		case Six: cout << "Six"; break;
		case Seven: cout << "Seven"; break;
		case Eight: cout << "Eight"; break;
		case Nine: cout << "Nine"; break;
		case Ten: cout << "Ten"; break;
		case Jack: cout << "Jack"; break;
		case Queen: cout << "Queen"; break;
		case King: cout << "King"; break;
		case Ace: cout << "Ace"; break;
	};
	
	cout << " of ";
	switch (card.suit) {
		case Spades: cout << "Spades"; break;
		case Hearts: cout << "Hearts"; break;
		case Clubs: cout << "Clubs"; break;
		case Diamonds: cout << "Diamonds"; break;
	};

	cout << "\n" ;
}

Card ChangeCard(char card) {
	switch (card) {
	case a:  Card a;
	case b:  Card b;
	case c:  Card c;
	case d:  Card d;
	};
}

	//I decided to make the suit relevant by using them like this.
void HighCard(Card card1, Card card2) {
	
	if (card1.rank + card1.suit > card2.rank + card2.suit) {
		cout << "the higher of the two cards is: ";
		 PrintCard(card1);
	}
	else if (card1.rank + card1.suit < card2.rank + card2.suit) {
		cout << "the higher of the two cards is: ";
		 PrintCard(card2);
	}
	else {
		cout << "The two cards are of equale value";
	}
}



int main()
{
	char x;
	char y;
	Card card1;
	Card card2;

	Card a;
	a.rank = Jack;
	a.suit = Hearts;
	PrintCard(a);

	Card b;
	b.rank = Ace;
	b.suit = Spades;
	PrintCard(b);

	Card c;
	c.rank = Ace;
	c.suit = Diamonds;
	PrintCard(c);

	Card d;
	d.rank = Three;
	d.suit = Diamonds;
	PrintCard(d);

	cout << "Please select two of either a, b, c or d for the the corresponding card" << "\n";
	cin >> x >> y;
	Card (ChangeCard(x));
	Card(ChangeCard(y));
	Card card1 = Card (ChangeCard(x));
	Card card2 = Card (ChangeCard(y));
	HighCard(card1, card2);

	(void)_getch();
	return 0;
}
